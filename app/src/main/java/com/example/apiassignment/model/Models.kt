package com.example.apiassignment.model

import org.joda.time.LocalTime


// Regex to remove @ColumnInfo @ColumnInfo\(name = "([a-z].*")\)

data class LoginModel constructor(
    var id: Int? = null,
     var accessToken: String,
     var scope: String,
     var tokenType: String
)

data class ProjectsModel constructor(
    var id: Int,
    var name: String,
    var description: String,
    var createdOn: LocalTime,
    var sshUrl: String,
    var httpUrl: String,
    var stars: String,
    var forks: String,
    var lastActivity: String,
    var visibility: String,
    var isIssuesEnabled: Boolean,
    var isWikiEnabled: Boolean,
    var isJobsEnabled: Boolean,
    var isMergeRequestsEnabled: Boolean
)

data class UserModel constructor(
    var id: Int,
    var name: String,
    var userName: String,
    var email: String,
    var avatarUrl: String,
    var bio: String,
    var lastActivity: String,
    var createdAt: LocalTime
)