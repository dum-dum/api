package com.example.apiassignment.ui.utils

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar

fun String?.string(): String {
    return if (!this.isNullOrBlank()) this.toString() else "null value supplied!"
}

fun Activity.showSnackBar(
    msg: String,
    view: View = (this.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0),
    actionMsg: String? = "Ok", listener: SnackBarClickListener? = null
) {
    Snackbar.make(view, msg, Snackbar.LENGTH_LONG).apply {
        this.setAction(actionMsg) {
            listener?.onActionClick(this)
        }
    }.show()
}

fun Activity.showShortToast(msg: String) {
    Toast.makeText(this,msg,Toast.LENGTH_SHORT).show()
}
fun Activity.showLongToast(msg: String) {
    Toast.makeText(this,msg,Toast.LENGTH_LONG).show()
}
