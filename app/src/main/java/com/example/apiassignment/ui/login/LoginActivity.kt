package com.example.apiassignment.ui.login

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.apiassignment.R
import com.example.apiassignment.ui.core.BaseActivity
import com.example.apiassignment.ui.core.BaseViewModel
import com.example.apiassignment.ui.utils.ExceptionHandler
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject


class LoginActivity : BaseActivity() {

    private lateinit var loginViewModel: LoginViewModel
    private lateinit var rootLayout: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        loginViewModel =
            ViewModelProviders.of(this, viewModelFactory)[LoginViewModel::class.java]
        rootLayout = findViewById(R.id.layout_main_activity)
        baseViewModel = loginViewModel
        attachObserver()

        login_fab.setOnClickListener {
            loadDashboard()
        }
    }

    private fun attachObserver() {
        loginViewModel.mainLiveData.observe(this, Observer {
            Log.d(javaClass.simpleName, "$it")
        })
    }

    private fun loadDashboard() {
        if (!loginUsername.text.isNullOrBlank() and !loginPassword.text.isNullOrBlank()) {
            loginViewModel.receiveAuthKey(
                loginUsername.text.toString(),
                loginPassword.text.toString()
            )

        }
    }
}
