package com.example.apiassignment.ui.utils

import com.google.android.material.snackbar.Snackbar

interface SnackBarClickListener {
    fun onActionClick(snackbar: Snackbar)
}