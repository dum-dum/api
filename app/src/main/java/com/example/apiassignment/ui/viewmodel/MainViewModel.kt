package com.example.apiassignment.ui.viewmodel

import android.util.Log
import androidx.lifecycle.MediatorLiveData
import com.example.apiassignment.data.login.local.LoginEntity
import com.example.apiassignment.data.login.local.LoginRepository
import com.example.apiassignment.data.login.remote.LoginAuthApi
import com.example.apiassignment.data.login.remote.LoginAuthPojo
import com.example.apiassignment.ui.core.BaseViewModel
import com.example.apiassignment.ui.utils.ExceptionHandler
import com.example.apiassignment.ui.utils.string
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val loginAuthApi: LoginAuthApi,
    private val loginRepository: LoginRepository
) : BaseViewModel() {

    val mainLiveData = MediatorLiveData<LoginAuthPojo>()

    fun receiveAuthKey() = safeExecution({
        loginAuthApi.getAuthKey("password", "ddesai@bosleo.com", "12345678")
    }, {
        Log.e(javaClass.simpleName, "$it")
        insertLoginData(it.accessToken.string(), it.scope.string(), it.tokenType.string())
    }, {
        errorLiveData.postValue(
            ExceptionHandler.HttpUnauthorizedException(it)
        )
    })


    private fun insertLoginData(accessToken: String, scope: String, tokenType: String) =
        safeExecution({
            loginRepository.insert(LoginEntity(accessToken, scope, tokenType))
        }, {num ->
            Log.e(javaClass.simpleName,"Insert: $num")
        }, {
it.printStackTrace()
        })


}