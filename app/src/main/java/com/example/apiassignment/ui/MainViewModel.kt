package com.example.apiassignment.ui

import android.util.Log
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.apiassignment.data.LoginAuthApi
import com.example.apiassignment.data.LoginAuthPojo
import com.example.apiassignment.data.UserDataPojo
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(private val loginAuthApi: LoginAuthApi) : ViewModel() {

    val mainLiveData = MediatorLiveData<LoginAuthPojo>()
    val userLiveData = MediatorLiveData<UserDataPojo>()


    fun receiveAuthKey() {
        viewModelScope.launch {
            try {
                loginAuthApi.getAuthKey("password", "ddesai@bosleo.com", "12345678").apply {
                    receiveUserInfo(this.accessToken)
                }
            } catch (e: Exception) {

            }

        }
    }

    private fun receiveUserInfo(accessToken: String?) {
        viewModelScope.launch {
            try {
                userLiveData.value =
                    loginAuthApi.getUserDetail(mapOf(Pair("Authorization", "Bearer $accessToken")))
            } catch (e: Exception) {
                Log.d("ERROR", "$e")
            }

        }
    }
}