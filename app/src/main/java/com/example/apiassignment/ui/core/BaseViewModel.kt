package com.example.apiassignment.ui.core

import androidx.lifecycle.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

open class BaseViewModel : ViewModel(), CoroutineScope {
    val errorLiveData: MutableLiveData<Exception> by lazy { MutableLiveData<Exception>() }

    private val job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.IO + job

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }

    fun <T> safeExecution(
        resultLiveData: MediatorLiveData<T>,
        block: () -> LiveData<T>
    ) {
        viewModelScope.launch {
            try {
                resultLiveData.addSource(block.invoke()) { t ->
                    resultLiveData.postValue(t)
                }
            } catch (e: Exception) {
                errorLiveData.postValue(e)
            }
        }
    }

    fun <T> safeExecution(resultLiveData: MutableLiveData<T>, block: () -> T) {
        viewModelScope.launch {
            try {
                resultLiveData.postValue(block.invoke())
            } catch (e: Exception) {
                errorLiveData.postValue(e)
            }
        }
    }

    fun <T> safeExecution(
        resultLiveData: MutableLiveData<T>,
        errorLiveData: MutableLiveData<Exception>,
        block: () -> T
    ) {
        viewModelScope.launch {
            try {
                resultLiveData.postValue(block.invoke())
            } catch (e: Exception) {
                errorLiveData.postValue(e)
            }
        }
    }

    fun <T> safeExecutionOnBackgroundThread(
        block: suspend () -> T,
        success: (T) -> Unit,
        error: (Exception) -> Unit
    ) {
        viewModelScope.launch(Dispatchers.IO) {
            try {
                success(block.invoke())
            } catch (e: Exception) {
                error(e)
            }
        }
    }

    fun <T> safeExecutionOnMainThread(
        block: suspend () -> T,
        success: (T) -> Unit,
        error: (Exception) -> Unit
    ) {
        viewModelScope.launch(Dispatchers.Main) {
            try {
                success(block.invoke())
            } catch (e: Exception) {
                error(e)
            }
        }
    }

}

