package com.example.apiassignment.ui.login

import android.util.Log
import androidx.lifecycle.MediatorLiveData
import com.example.apiassignment.data.login.datasource.local.LoginLocalSource
import com.example.apiassignment.api.LoginAuthPojo
import com.example.apiassignment.ui.core.BaseViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val loginLocalSource: LoginLocalSource) :
    BaseViewModel() {

    val mainLiveData = MediatorLiveData<LoginAuthPojo>()

//    fun receiveAuthKey(username: String, password: String) =
//        safeExecutionOnBackgroundThread({
//            loginLocalSource.doLogin(username, password)
//        },
//            {
//                Log.e(javaClass.simpleName, "Login Result $it = Passed")
//
//            }, {
//                errorLiveData.postValue(
//                    ExceptionHandler.HttpUnauthorizedException(it)
//                )
//            })

    fun receiveAuthKey(username:String, password: String){
        launch {
            loginLocalSource.doLogin(username, password).either({
                errorLiveData.postValue(it)

            },{
                Log.d(javaClass.simpleName,"Api Called and Inserted $it")
            })
        }
    }
}