package com.example.apiassignment.ui.core

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.apiassignment.ui.utils.ExceptionHandler
import com.example.apiassignment.ui.utils.showShortToast
import com.example.apiassignment.ui.utils.showSnackBar
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

@SuppressLint("Registered")
open class BaseActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var baseViewModel: BaseViewModel


    sealed class ExceptionHandlerType(val message: String, val throwable: Throwable) {
        class SnackbarError(message: String, throwable: Throwable) :
            ExceptionHandlerType(message, throwable)

        class LogOnlyError(message: String, throwable: Throwable) :
            ExceptionHandlerType(message, throwable)

        class ToastError(message: String, throwable: Throwable) :
            ExceptionHandlerType(message, throwable)
    }

    override fun onResume() {
        super.onResume()
        baseViewModel.errorLiveData.observe(this, Observer {
            it?.apply {
                when (val type = handleException(it)) {
                    is ExceptionHandlerType.SnackbarError -> {
                        Log.e("Snackbar Error:", type.message, type.throwable)
                        showSnackBar(type.message)
                    }
                    is ExceptionHandlerType.LogOnlyError -> {
                        Log.e("Log only Error", type.message, type.throwable)
                    }
                    is ExceptionHandlerType.ToastError -> {
                        Log.e("Toast Error:", type.message, type.throwable)
                        showShortToast(type.message)
                    }

                }
            }
        })
    }

    override fun onPause() {
        super.onPause()
        baseViewModel.errorLiveData.value = null
    }


    private fun handleException(exception: Exception): ExceptionHandlerType {
        return if (exception is ExceptionHandler) {
            return when (exception) {
                is ExceptionHandler.UnknownException -> {
                    ExceptionHandlerType.LogOnlyError(
                        exception.throwable.localizedMessage.orEmpty(),
                        exception.throwable
                    )
                }
                is ExceptionHandler.InvalidAuthKey -> {
                    ExceptionHandlerType.SnackbarError(
                        "Invalid username or password",
                        exception.throwable
                    )
                }
                else -> ExceptionHandlerType.LogOnlyError("ERROR", exception)
            }
        } else {
            ExceptionHandlerType.LogOnlyError("ERROR", exception)
        }
    }

}