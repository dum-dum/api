package com.example.apiassignment.ui.utils

import androidx.room.TypeConverter
import org.joda.time.LocalTime


class TypeConverter {

    @TypeConverter
    fun toOffsetDateTime(value: Int): LocalTime {
        return LocalTime().withMillisOfDay(value)
    }

    @TypeConverter
    fun fromOffsetDateTime(date: LocalTime): Int {
        return date.millisOfDay
    }
}
