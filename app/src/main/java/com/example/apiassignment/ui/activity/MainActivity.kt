package com.example.apiassignment.ui.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.apiassignment.R
import com.example.apiassignment.ui.utils.ExceptionHandler
import com.example.apiassignment.ui.utils.string
import com.example.apiassignment.ui.viewmodel.MainViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var mainViewModel: MainViewModel
    private lateinit var rootLayout: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel =
            ViewModelProviders.of(this, viewModelFactory)[MainViewModel::class.java]
        rootLayout = findViewById(R.id.layout_main_activity)

        attachObserver()
        mainViewModel.receiveAuthKey()

    }
    private fun attachObserver() {
        mainViewModel.mainLiveData.observe(this, Observer {
           Log.d("TAG", "$it")
        })
        mainViewModel.errorLiveData.observe(this, Observer {
            val error = if(it is ExceptionHandler){
                it.msg
            }else{
                it.localizedMessage
            }
            Snackbar.make(rootLayout,error.toString(),Snackbar.LENGTH_LONG).show()
        })
    }
}
