package com.example.apiassignment.ui

import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.apiassignment.R
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel =
            ViewModelProviders.of(this, viewModelFactory)[MainViewModel::class.java]

        attachObserver()
        mainViewModel.receiveAuthKey()

    }
    private fun attachObserver() {
        mainViewModel.mainLiveData.observe(this, Observer {
           Log.d("TAG", "$it")
        })
        mainViewModel.userLiveData.observe(this, Observer {
            Log.d("TAG", "$it")
        })
    }
}
