package com.example.apiassignment.ui.utils


sealed class ExceptionHandler(val throwable: Throwable, val msg: String = "") : Exception() {
    class InvalidAuthKey(throwable: Throwable, msg: String) : ExceptionHandler(throwable)
    class HttpUnauthorizedException(throwable: Throwable) :
        ExceptionHandler(throwable, "Unauthorized Access! Please login again!")

    class APICallingError(throwable: Throwable) : ExceptionHandler(throwable, "Api calling failed!")
    class RoomDatabaseError(throwable: Throwable) :
        ExceptionHandler(throwable, "Error with Room Database")

    class UnknownHostException(throwable: Throwable): ExceptionHandler(throwable,"Could not determine IP address of host!")

    class UnknownException(throwable: Throwable): ExceptionHandler(throwable,"Unknown Exception Caught!!!")
}