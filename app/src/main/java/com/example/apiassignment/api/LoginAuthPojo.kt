package com.example.apiassignment.api


import com.example.apiassignment.data.login.datasource.local.LoginEntity
import com.example.apiassignment.ui.utils.string
import com.google.gson.annotations.SerializedName

data class LoginAuthPojo(
    @SerializedName("access_token")
    var accessToken: String?,
    @SerializedName("created_at")
    var createdAt: Int?,
    @SerializedName("refresh_token")
    var refreshToken: String?,
    @SerializedName("scope")
    var scope: String?,
    @SerializedName("token_type")
    var tokenType: String?
) {
    fun toLoginEntity() = LoginEntity(accessToken.string(),scope.string(),tokenType.string())
}