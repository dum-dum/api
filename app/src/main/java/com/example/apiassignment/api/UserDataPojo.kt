package com.example.apiassignment.api


import com.google.gson.annotations.SerializedName

data class UserDataPojo(
    @SerializedName("avatar_url")
    var avatarUrl: String?,
    @SerializedName("bio")
    var bio: Any?,
    @SerializedName("can_create_group")
    var canCreateGroup: Boolean?,
    @SerializedName("can_create_project")
    var canCreateProject: Boolean?,
    @SerializedName("color_scheme_id")
    var colorSchemeId: Int?,
    @SerializedName("confirmed_at")
    var confirmedAt: String?,
    @SerializedName("created_at")
    var createdAt: String?,
    @SerializedName("current_sign_in_at")
    var currentSignInAt: String?,
    @SerializedName("email")
    var email: String?,
    @SerializedName("external")
    var `external`: Boolean?,
    @SerializedName("id")
    var id: Int?,
    @SerializedName("identities")
    var identities: List<Any?>?,
    @SerializedName("last_activity_on")
    var lastActivityOn: String?,
    @SerializedName("last_sign_in_at")
    var lastSignInAt: String?,
    @SerializedName("linkedin")
    var linkedin: String?,
    @SerializedName("location")
    var location: Any?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("organization")
    var organization: Any?,
    @SerializedName("private_profile")
    var privateProfile: Any?,
    @SerializedName("projects_limit")
    var projectsLimit: Int?,
    @SerializedName("public_email")
    var publicEmail: String?,
    @SerializedName("skype")
    var skype: String?,
    @SerializedName("state")
    var state: String?,
    @SerializedName("theme_id")
    var themeId: Int?,
    @SerializedName("twitter")
    var twitter: String?,
    @SerializedName("two_factor_enabled")
    var twoFactorEnabled: Boolean?,
    @SerializedName("username")
    var username: String?,
    @SerializedName("web_url")
    var webUrl: String?,
    @SerializedName("website_url")
    var websiteUrl: String?
)