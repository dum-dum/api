package com.example.apiassignment.data.login.datasource.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.apiassignment.data.core.BaseDao

@Dao
interface LoginDao: BaseDao<LoginEntity> {
    @Transaction
    @Query("select * from login")
    fun getAll(): LiveData<List<LoginEntity>>

    @Transaction
    @Query("delete from login")
    fun deleteAll(): Int
}