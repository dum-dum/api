package com.example.apiassignment.data.core

object TableMeta {
    object Login {
        const val LOGIN = "LOGIN"
        const val ID = "ID"
        const val ACCESSTOKEN = "ACCESS_TOKEN"
        const val SCOPE = "SCOPE"
        const val TOKENTYPE = "TOKEN_TYPE"
    }

    object User {
        const val USER = "USER"
        const val ID = "ID"
        const val NAME = "NAME"
        const val USERNAME = "username"
        const val EMAIL = "EMAIL"
        const val AVATARURL = "AVATAR_URL"
        const val BIO = "BIO"
        const val LASTACTIVITY = "LAST_ACTIVITY"
        const val CREATEDAT = "CREATED_AT"
    }

    object Project {
        const val PROJECT = "PROJECT"
        const val ID = "ID"
        const val NAME = "name"
        const val DESCRIPTION = "description"
        const val CREATEDON = "CREATED_ON"
        const val SSHURL = "ssh_url"
        const val HTTPURL = "HTTP_URL"
        const val STARS = "STARS"
        const val FORKS = "FORK"
        const val LASTACTIVITY = "LAST_ACTIVITY"
        const val VISIBILITY = "VISIBILITY"
        const val ISISSUESENABLED = "IS_ISSUES_ENABLED"
        const val ISWIKIENABLED = "IS_WIKI_ENABLED"
        const val ISJOBSENABLED = "IS_JOBS_ENABLED"
        const val ISMERGEREQUESTSENABLED = "IS_MERGE_REQUESTS_ENABLED"

    }
}