package com.example.apiassignment.data


import com.google.gson.annotations.SerializedName

data class LoginAuthPojo(
    @SerializedName("access_token")
    var accessToken: String?,
    @SerializedName("created_at")
    var createdAt: Int?,
    @SerializedName("refresh_token")
    var refreshToken: String?,
    @SerializedName("scope")
    var scope: String?,
    @SerializedName("token_type")
    var tokenType: String?
)