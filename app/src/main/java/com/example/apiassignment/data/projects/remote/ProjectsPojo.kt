package com.example.apiassignment.data.projects.remote


import com.google.gson.annotations.SerializedName

data class ProjectsPojo(
    @SerializedName("archived")
    var archived: Boolean?,
    @SerializedName("avatar_url")
    var avatarUrl: String?,
    @SerializedName("ci_config_path")
    var ciConfigPath: Any?,
    @SerializedName("container_registry_enabled")
    var containerRegistryEnabled: Boolean?,
    @SerializedName("created_at")
    var createdAt: String?,
    @SerializedName("creator_id")
    var creatorId: Int?,
    @SerializedName("default_branch")
    var defaultBranch: String?,
    @SerializedName("description")
    var description: String?,
    @SerializedName("forked_from_project")
    var forkedFromProject: ForkedFromProject?,
    @SerializedName("forks_count")
    var forksCount: Int?,
    @SerializedName("http_url_to_repo")
    var httpUrlToRepo: String?,
    @SerializedName("id")
    var id: Int?,
    @SerializedName("import_status")
    var importStatus: String?,
    @SerializedName("issues_enabled")
    var issuesEnabled: Boolean?,
    @SerializedName("jobs_enabled")
    var jobsEnabled: Boolean?,
    @SerializedName("last_activity_at")
    var lastActivityAt: String?,
    @SerializedName("lfs_enabled")
    var lfsEnabled: Boolean?,
    @SerializedName("_links")
    var links: Links?,
    @SerializedName("merge_method")
    var mergeMethod: String?,
    @SerializedName("merge_requests_enabled")
    var mergeRequestsEnabled: Boolean?,
    @SerializedName("name")
    var name: String?,
    @SerializedName("name_with_namespace")
    var nameWithNamespace: String?,
    @SerializedName("namespace")
    var namespace: Namespace?,
    @SerializedName("only_allow_merge_if_all_discussions_are_resolved")
    var onlyAllowMergeIfAllDiscussionsAreResolved: Boolean?,
    @SerializedName("only_allow_merge_if_pipeline_succeeds")
    var onlyAllowMergeIfPipelineSucceeds: Boolean?,
    @SerializedName("open_issues_count")
    var openIssuesCount: Int?,
    @SerializedName("path")
    var path: String?,
    @SerializedName("path_with_namespace")
    var pathWithNamespace: String?,
    @SerializedName("permissions")
    var permissions: Permissions?,
    @SerializedName("printing_merge_request_link_enabled")
    var printingMergeRequestLinkEnabled: Boolean?,
    @SerializedName("public_jobs")
    var publicJobs: Boolean?,
    @SerializedName("readme_url")
    var readmeUrl: String?,
    @SerializedName("request_access_enabled")
    var requestAccessEnabled: Boolean?,
    @SerializedName("resolve_outdated_diff_discussions")
    var resolveOutdatedDiffDiscussions: Boolean?,
    @SerializedName("shared_runners_enabled")
    var sharedRunnersEnabled: Boolean?,
    @SerializedName("shared_with_groups")
    var sharedWithGroups: List<SharedWithGroup?>?,
    @SerializedName("snippets_enabled")
    var snippetsEnabled: Boolean?,
    @SerializedName("ssh_url_to_repo")
    var sshUrlToRepo: String?,
    @SerializedName("star_count")
    var starCount: Int?,
    @SerializedName("tag_list")
    var tagList: List<Any?>?,
    @SerializedName("visibility")
    var visibility: String?,
    @SerializedName("web_url")
    var webUrl: String?,
    @SerializedName("wiki_enabled")
    var wikiEnabled: Boolean?
) {
    data class ForkedFromProject(
        @SerializedName("avatar_url")
        var avatarUrl: Any?,
        @SerializedName("created_at")
        var createdAt: String?,
        @SerializedName("default_branch")
        var defaultBranch: String?,
        @SerializedName("description")
        var description: String?,
        @SerializedName("forks_count")
        var forksCount: Int?,
        @SerializedName("http_url_to_repo")
        var httpUrlToRepo: String?,
        @SerializedName("id")
        var id: Int?,
        @SerializedName("last_activity_at")
        var lastActivityAt: String?,
        @SerializedName("name")
        var name: String?,
        @SerializedName("name_with_namespace")
        var nameWithNamespace: String?,
        @SerializedName("namespace")
        var namespace: Namespace?,
        @SerializedName("path")
        var path: String?,
        @SerializedName("path_with_namespace")
        var pathWithNamespace: String?,
        @SerializedName("readme_url")
        var readmeUrl: String?,
        @SerializedName("ssh_url_to_repo")
        var sshUrlToRepo: String?,
        @SerializedName("star_count")
        var starCount: Int?,
        @SerializedName("tag_list")
        var tagList: List<Any?>?,
        @SerializedName("web_url")
        var webUrl: String?
    ) {
        data class Namespace(
            @SerializedName("full_path")
            var fullPath: String?,
            @SerializedName("id")
            var id: Int?,
            @SerializedName("kind")
            var kind: String?,
            @SerializedName("name")
            var name: String?,
            @SerializedName("parent_id")
            var parentId: Int?,
            @SerializedName("path")
            var path: String?
        )
    }

    data class Links(
        @SerializedName("events")
        var events: String?,
        @SerializedName("issues")
        var issues: String?,
        @SerializedName("labels")
        var labels: String?,
        @SerializedName("members")
        var members: String?,
        @SerializedName("merge_requests")
        var mergeRequests: String?,
        @SerializedName("repo_branches")
        var repoBranches: String?,
        @SerializedName("self")
        var self: String?
    )

    data class Namespace(
        @SerializedName("full_path")
        var fullPath: String?,
        @SerializedName("id")
        var id: Int?,
        @SerializedName("kind")
        var kind: String?,
        @SerializedName("name")
        var name: String?,
        @SerializedName("parent_id")
        var parentId: Any?,
        @SerializedName("path")
        var path: String?
    )

    data class Permissions(
        @SerializedName("group_access")
        var groupAccess: GroupAccess?,
        @SerializedName("project_access")
        var projectAccess: Any?
    ) {
        data class GroupAccess(
            @SerializedName("access_level")
            var accessLevel: Int?,
            @SerializedName("notification_level")
            var notificationLevel: Int?
        )
    }

    data class SharedWithGroup(
        @SerializedName("expires_at")
        var expiresAt: Any?,
        @SerializedName("group_access_level")
        var groupAccessLevel: Int?,
        @SerializedName("group_full_path")
        var groupFullPath: String?,
        @SerializedName("group_id")
        var groupId: Int?,
        @SerializedName("group_name")
        var groupName: String?
    )
}