package com.example.apiassignment.data.projects.local

import androidx.lifecycle.LiveData

interface ProjectsRepository {
    fun getAll(): LiveData<List<ProjectsEntity>>
    fun insert(projectsEntity: ProjectsEntity) : Long
    fun deleteAll() : Int
}