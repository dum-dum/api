package com.example.apiassignment.data.projects.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.example.apiassignment.data.core.BaseDao
import com.example.apiassignment.data.core.TableMeta

@Dao
interface ProjectsDao : BaseDao<ProjectsEntity> {

    @Transaction
    @Query("select * from ${TableMeta.Project.PROJECT}")
    fun getAll(): LiveData<List<ProjectsEntity>>

    @Transaction
    @Query("Delete from ${TableMeta.Project.PROJECT}")
    fun deleteAll(): Int
}