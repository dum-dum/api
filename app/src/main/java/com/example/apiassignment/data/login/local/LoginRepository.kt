package com.example.apiassignment.data.login.local

import androidx.lifecycle.LiveData

interface LoginRepository {
    fun getAll(): LiveData<List<LoginEntity>>
    fun insert(loginEntity: LoginEntity): Long
    fun deleteAll(): Int
}