package com.example.apiassignment.data.login.contract

import com.example.apiassignment.data.core.BaseDataRepository
import com.example.apiassignment.data.login.datasource.local.LoginLocalSource
import com.example.apiassignment.data.login.datasource.remote.LoginRemoteSource
import com.example.apiassignment.ui.utils.Either

class LoginDataRepo constructor(
    private val loginRemoteSource: LoginRemoteSource,
    private val loginLocalSource: LoginLocalSource
) : BaseDataRepository(), LoginRepo {

    override suspend fun doLogin(
        username: String,
        password: String,
        grantType: String
    ): Either<Exception, Boolean> {
        return either {
            with(loginRemoteSource.doLogin(username, password, grantType)) {
                loginLocalSource.insert(this.toLoginEntity())
                true
            }
        }
    }
}