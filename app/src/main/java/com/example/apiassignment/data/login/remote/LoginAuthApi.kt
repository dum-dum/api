package com.example.apiassignment.data.login.remote

import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface LoginAuthApi {

    @FormUrlEncoded
    @POST("/oauth/token")
    suspend fun getAuthKey(
        @Field("grant_type") grantType: String,
        @Field("username") username: String,
        @Field("password") password: String
    ): LoginAuthPojo
}