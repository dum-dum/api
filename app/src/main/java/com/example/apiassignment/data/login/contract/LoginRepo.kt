package com.example.apiassignment.data.login.contract

import com.example.apiassignment.ui.utils.Either
import java.lang.Exception

interface LoginRepo {
    suspend fun doLogin(username: String, password: String, grantType: String = "password") : Either<Exception,Boolean>
}