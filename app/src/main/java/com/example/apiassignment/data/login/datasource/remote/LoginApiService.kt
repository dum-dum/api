package com.example.apiassignment.data.login.datasource.remote

import com.example.apiassignment.api.LoginAuthPojo
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface LoginApiService {

    @FormUrlEncoded
    @POST("/oauth/token")
    suspend fun getAuthKey(
        @Field("grant_type") grantType: String,
        @Field("username") username: String,
        @Field("password") password: String
    ): LoginAuthPojo
}