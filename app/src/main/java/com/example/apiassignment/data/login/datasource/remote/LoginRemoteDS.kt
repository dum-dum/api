package com.example.apiassignment.data.login.datasource.remote

import com.example.apiassignment.api.LoginAuthPojo


class LoginRemoteDS(private val loginApiService: LoginApiService) : LoginRemoteSource {

    override suspend fun doLogin(
        username: String,
        password: String,
        grantType: String
    ): LoginAuthPojo = loginApiService.getAuthKey(grantType, username, password)


}