package com.example.apiassignment.data.projects.local

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class ProjectsRepositoryIMPL (private val projectsDao: ProjectsDao): ProjectsRepository {

    @WorkerThread
    override fun getAll(): LiveData<List<ProjectsEntity>> {
        return projectsDao.getAll()
    }

    @WorkerThread
    override fun insert(projectsEntity: ProjectsEntity): Long {
        return projectsDao.insert(projectsEntity)
    }

    @WorkerThread
    override fun deleteAll(): Int {
        return projectsDao.deleteAll()
    }

}