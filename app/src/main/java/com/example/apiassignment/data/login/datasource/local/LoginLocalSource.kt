package com.example.apiassignment.data.login.datasource.local

import androidx.lifecycle.LiveData
import com.example.apiassignment.model.LoginModel
import com.example.apiassignment.ui.utils.Either

interface LoginLocalSource {
    fun getAll(): LiveData<List<LoginEntity>>
    fun insert(loginEntity: LoginEntity): Long
    fun deleteAll(): Int
}