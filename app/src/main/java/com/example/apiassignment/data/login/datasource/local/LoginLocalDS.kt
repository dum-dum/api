package com.example.apiassignment.data.login.datasource.local

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.example.apiassignment.data.core.BaseDataRepository
import com.example.apiassignment.data.login.datasource.remote.LoginApiService
import com.example.apiassignment.model.LoginModel
import com.example.apiassignment.ui.utils.Either
import com.example.apiassignment.ui.utils.string

class LoginLocalDS(
    private val loginDao: LoginDao
) :
    LoginLocalSource, BaseDataRepository() {

    @WorkerThread
    override fun getAll(): LiveData<List<LoginEntity>> {
        try {

        } catch (e: Exception) {

        }
        return loginDao.getAll()
    }

    @WorkerThread
    override fun insert(loginEntity: LoginEntity): Long {
        return loginDao.insert(loginEntity)
    }

    @WorkerThread
    override fun deleteAll(): Int {
        return loginDao.deleteAll()
    }
}