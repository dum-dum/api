package com.example.apiassignment.data.login.datasource.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.apiassignment.data.core.TableMeta
import com.example.apiassignment.model.LoginModel

@Entity(tableName = TableMeta.Login.LOGIN)
data class LoginEntity constructor(
    @ColumnInfo(name = TableMeta.Login.ACCESSTOKEN) var accessToken: String,
    @ColumnInfo(name = TableMeta.Login.SCOPE) var scope: String,
    @ColumnInfo(name = TableMeta.Login.TOKENTYPE) var tokenType: String,
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = TableMeta.Login.ID) var id: Int? = null
) {
    fun mapToViewModel() = LoginModel(id ?: -1,accessToken,scope,tokenType)
}