package com.example.apiassignment.data.login.local

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class LoginRepositoryIMPL(private val loginDao: LoginDao) :
    LoginRepository {

    @WorkerThread
    override fun getAll(): LiveData<List<LoginEntity>> {
        return loginDao.getAll()
    }

    @WorkerThread
    override fun insert(loginEntity: LoginEntity): Long {
        return loginDao.insert(loginEntity)
    }

    @WorkerThread
    override fun deleteAll(): Int {
        return loginDao.deleteAll()
    }

}