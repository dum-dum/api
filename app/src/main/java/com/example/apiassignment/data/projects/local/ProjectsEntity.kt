package com.example.apiassignment.data.projects.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.apiassignment.data.core.TableMeta
import com.example.apiassignment.model.ProjectsModel
import org.joda.time.LocalTime

@Entity(tableName = TableMeta.Project.PROJECT)

data class ProjectsEntity constructor(
    @ColumnInfo(name = TableMeta.Project.NAME ) var name: String,
    @ColumnInfo(name = TableMeta.Project.DESCRIPTION) var description: String,
    @ColumnInfo(name = TableMeta.Project.CREATEDON) var createdOn: LocalTime,
    @ColumnInfo(name = TableMeta.Project.SSHURL) var sshUrl: String,
    @ColumnInfo(name = TableMeta.Project.HTTPURL) var httpUrl: String,
    @ColumnInfo(name = TableMeta.Project.STARS) var stars: String,
    @ColumnInfo(name = TableMeta.Project.FORKS) var forks: String,
    @ColumnInfo(name = TableMeta.Project.LASTACTIVITY) var lastActivity: String,
    @ColumnInfo(name = TableMeta.Project.VISIBILITY) var visibility: String,
    @ColumnInfo(name = TableMeta.Project.ISISSUESENABLED) var isIssuesEnabled: Boolean,
    @ColumnInfo(name = TableMeta.Project.ISWIKIENABLED) var isWikiEnabled: Boolean,
    @ColumnInfo(name = TableMeta.Project.ISJOBSENABLED) var isJobsEnabled: Boolean,
    @ColumnInfo(name = TableMeta.Project.ISMERGEREQUESTSENABLED) var isMergeRequestsEnabled: Boolean,
    @PrimaryKey @ColumnInfo(name = TableMeta.Project.ID) var id: Int? = null
) {
    fun mapToViewModel() = ProjectsModel(
        id ?: -1,
        name,
        description,
        createdOn,
        sshUrl,
        httpUrl,
        stars,
        forks,
        lastActivity,
        visibility,
        isIssuesEnabled,
        isWikiEnabled,
        isJobsEnabled,
        isMergeRequestsEnabled
    )
}