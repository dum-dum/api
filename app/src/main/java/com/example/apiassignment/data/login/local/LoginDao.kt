package com.example.apiassignment.data.login.local

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface LoginDao {
    @Transaction
    @Query("select * from login")
    fun getAll(): LiveData<List<LoginEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(loginEntity: LoginEntity) : Long

    @Transaction
    @Query("delete from login")
    fun deleteAll(): Int
}