package com.example.apiassignment.data

import retrofit2.http.*

interface LoginAuthApi {

    @FormUrlEncoded
    @POST("/oauth/token")
    suspend fun getAuthKey(@Field("grant_type") grantType: String,
                           @Field("username") username: String,
                           @Field("password") password: String) : LoginAuthPojo


    @GET("/api/v4/user")
    suspend fun getUserDetail(@HeaderMap headers:Map<String,String>) : UserDataPojo
}