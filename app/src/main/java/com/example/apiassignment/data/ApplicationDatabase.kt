package com.example.apiassignment.data

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.apiassignment.data.login.datasource.local.LoginDao
import com.example.apiassignment.data.login.datasource.local.LoginEntity
import com.example.apiassignment.data.projects.local.ProjectsDao
import com.example.apiassignment.data.projects.local.ProjectsEntity
import com.example.apiassignment.data.user.local.UserDao
import com.example.apiassignment.data.user.local.UserEntity
import com.example.apiassignment.ui.utils.TypeConverter

@TypeConverters(TypeConverter::class)
@Database(entities = [LoginEntity::class, ProjectsEntity::class, UserEntity::class], version = 1)
abstract class ApplicationDatabase : RoomDatabase() {
    //define DAOs here
    abstract val loginDao: LoginDao
    abstract val projectsDao: ProjectsDao
    abstract val userDao: UserDao
}