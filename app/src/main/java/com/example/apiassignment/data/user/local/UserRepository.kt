package com.example.apiassignment.data.user.local

import androidx.lifecycle.LiveData

interface UserRepository {
    fun getAll(): LiveData<List<UserEntity>>
    fun insert(userEntity: UserEntity) : Long
    fun deleteAll(): Int
}