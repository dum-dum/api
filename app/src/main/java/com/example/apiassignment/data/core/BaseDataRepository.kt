package com.example.apiassignment.data.core

import com.example.apiassignment.ui.utils.Either
import com.example.apiassignment.ui.utils.ExceptionHandler
import retrofit2.HttpException
import java.net.UnknownHostException

open class BaseDataRepository {

    inline fun <Result> either(execution: () -> Result): Either<Exception, Result> {
        return try {
            Either.Right(execution())
        } catch (e: Exception) {
            Either.Left(Exception(e.localizedMessage))
        }
    }

    inline fun <Result> either(
        executeIfConnected: () -> Unit,
        execution: () -> Result
    ): Either<Exception, Result> {
        try {
            executeIfConnected()
        } catch (e: Exception) {
            Either.Left(customExceptions(e))
        }

        return try {
            Either.Right(execution())
        } catch (e: Exception) {
            Either.Left(customExceptions(e))
        }
    }


    fun customExceptions(exception: Exception): ExceptionHandler {
        when (exception) {
            is UnknownHostException -> return ExceptionHandler.UnknownHostException(exception)
            is HttpException -> {
                when (exception.code()) {
                    401 -> return ExceptionHandler.HttpUnauthorizedException(exception)
                }
            }
            else -> return ExceptionHandler.UnknownException(exception)
        }
        return ExceptionHandler.UnknownException(exception)
    }
}