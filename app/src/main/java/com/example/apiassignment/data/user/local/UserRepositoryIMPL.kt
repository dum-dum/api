package com.example.apiassignment.data.user.local

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class UserRepositoryIMPL (private val userDao: UserDao) : UserRepository {

    @WorkerThread
    override fun getAll(): LiveData<List<UserEntity>> {
        return userDao.getAll()
    }

    @WorkerThread
    override fun insert(userEntity: UserEntity): Long {
        return userDao.insert(userEntity)
    }

    @WorkerThread
    override fun deleteAll(): Int {
        return userDao.deleteAll()
    }

}