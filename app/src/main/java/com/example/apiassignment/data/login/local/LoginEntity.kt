package com.example.apiassignment.data.login.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.apiassignment.model.LoginModel

@Entity(tableName = "login")
data class LoginEntity constructor(
    @ColumnInfo(name = "access_token") var accessToken: String,
    @ColumnInfo(name = "scope") var scope: String,
    @ColumnInfo(name = "token_type") var tokenType: String,
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Int? = null
) {
    fun mapToViewModel() = LoginModel(id ?: -1,accessToken,scope,tokenType)
}