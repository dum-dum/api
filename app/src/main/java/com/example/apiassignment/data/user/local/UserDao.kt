package com.example.apiassignment.data.user.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.example.apiassignment.data.core.BaseDao

@Dao
interface UserDao : BaseDao<UserEntity> {

    @Transaction
    @Query("Select * from user")
    fun getAll(): LiveData<List<UserEntity>>

    @Transaction
    @Query("delete from user")
    fun deleteAll(): Int
}