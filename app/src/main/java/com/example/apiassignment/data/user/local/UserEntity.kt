package com.example.apiassignment.data.user.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.apiassignment.data.core.TableMeta
import com.example.apiassignment.model.UserModel
import org.joda.time.LocalTime

@Entity(tableName = TableMeta.User.USER)

data class UserEntity constructor(
    @PrimaryKey @ColumnInfo(name = TableMeta.User.ID) var id: Int,
    @ColumnInfo(name = TableMeta.User.NAME) var name: String,
    @ColumnInfo(name = TableMeta.User.USERNAME) var userName: String,
    @ColumnInfo(name = TableMeta.User.EMAIL) var email: String,
    @ColumnInfo(name = TableMeta.User.AVATARURL) var avatarUrl: String,
    @ColumnInfo(name = TableMeta.User.BIO) var bio: String,
    @ColumnInfo(name = TableMeta.User.LASTACTIVITY) var lastActivity: String,
    @ColumnInfo(name = TableMeta.User.CREATEDAT) var createdAt: LocalTime
) {
    fun mapToViewModel() =
        UserModel(id, name, userName, email, avatarUrl, bio, lastActivity, createdAt)
}