package com.example.apiassignment.data.user.remote

import com.example.apiassignment.api.UserDataPojo
import retrofit2.http.*

interface UserApi {

    @GET("/api/v4/user")
    suspend fun getUserDetailWithHeader(@HeaderMap headers: Map<String, String>): UserDataPojo

    @GET("/api/v4/user")
    suspend fun getUserDetailWithUrl(@Query("access_token") accessToken: String): UserDataPojo
}