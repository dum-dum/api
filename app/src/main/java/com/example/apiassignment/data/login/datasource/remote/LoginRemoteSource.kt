package com.example.apiassignment.data.login.datasource.remote

import com.example.apiassignment.api.LoginAuthPojo
import com.example.apiassignment.model.LoginModel
import com.example.apiassignment.ui.utils.Either

interface LoginRemoteSource {

    suspend fun doLogin(username: String, password: String, grantType: String = "password"): LoginAuthPojo

}