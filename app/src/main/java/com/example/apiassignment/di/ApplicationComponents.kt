package com.example.apiassignment.di

import com.example.apiassignment.ApiAssignment
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AndroidSupportInjectionModule::class, ApplicationModule::class,
    ViewModelModule::class, NetworkModule::class, ActivityModule::class, DatabaseModule::class])
interface ApplicationComponents : AndroidInjector<ApiAssignment> {

    override fun inject(instance: ApiAssignment)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun apiAssignment(instance: ApiAssignment): Builder

        fun build(): ApplicationComponents
    }
}