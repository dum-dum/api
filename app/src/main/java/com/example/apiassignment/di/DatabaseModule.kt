package com.example.apiassignment.di

import android.content.Context
import androidx.room.Room
import com.example.apiassignment.data.ApplicationDatabase
import com.example.apiassignment.data.login.datasource.local.LoginDao
import com.example.apiassignment.data.login.datasource.local.LoginLocalSource
import com.example.apiassignment.data.login.datasource.local.LoginLocalDS
import com.example.apiassignment.data.login.datasource.remote.LoginApiService
import com.example.apiassignment.data.projects.local.ProjectsDao
import com.example.apiassignment.data.projects.local.ProjectsRepository
import com.example.apiassignment.data.projects.local.ProjectsRepositoryIMPL
import com.example.apiassignment.data.user.local.UserDao
import com.example.apiassignment.data.user.local.UserRepository
import com.example.apiassignment.data.user.local.UserRepositoryIMPL
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule{

    //Database Provider
    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext context: Context) : ApplicationDatabase {
        return Room.databaseBuilder(context,
            ApplicationDatabase::class.java, "GitLab API")
            .fallbackToDestructiveMigration()
            .build()
    }

    //Dao Providers
    @Singleton
    @Provides
    fun provideLoginDao(applicationDatabase: ApplicationDatabase): LoginDao {
        return applicationDatabase.loginDao
    }

    @Singleton
    @Provides
    fun provideProjectsDao(applicationDatabase: ApplicationDatabase) : ProjectsDao {
        return applicationDatabase.projectsDao
    }

    @Singleton
    @Provides
    fun provideUserDao(applicationDatabase: ApplicationDatabase) : UserDao {
        return applicationDatabase.userDao
    }

    //Repository Providers
    @Singleton
    @Provides
    fun provideLoginRepository(loginDao: LoginDao): LoginLocalSource {
        return LoginLocalDS(loginDao)
    }

    @Singleton
    @Provides
    fun provideProjectsRepository(projectsDao: ProjectsDao) : ProjectsRepository {
        return ProjectsRepositoryIMPL(projectsDao)
    }

    @Singleton
    @Provides
    fun provideUserRepository(userDao: UserDao) : UserRepository {
        return UserRepositoryIMPL(userDao)
    }
}