package com.example.apiassignment.di

import com.example.apiassignment.data.login.datasource.remote.LoginApiService
import com.example.apiassignment.data.user.remote.UserApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {
    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor())
    }

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        val okHttpClient = provideOkHttpClient()
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://192.168.2.130:8081/")
            .client(okHttpClient.build())
            .build()
    }

    @Singleton
    @Provides
    fun provideLoginClient(retrofit: Retrofit): LoginApiService {
        return retrofit.create(LoginApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideUserClient(retrofit: Retrofit): UserApi {
        return retrofit.create(UserApi::class.java)
    }

}