package com.example.apiassignment.di

import com.example.apiassignment.data.LoginAuthApi
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class CustomModules {
    @Singleton
    @Provides
    fun provideOkHttpClient(): OkHttpClient.Builder {
        return OkHttpClient.Builder()
        .addInterceptor(HttpLoggingInterceptor())
    }

    @Singleton
    @Provides
    fun provideRetrofit() : Retrofit {
        val okHttpClient = provideOkHttpClient()
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://192.168.2.130:8081/")
            .client(okHttpClient.build())
            .build()
    }

    @Singleton
    @Provides
    fun provideClient(retrofit: Retrofit) : LoginAuthApi {
        return retrofit.create(LoginAuthApi::class.java)
    }

}