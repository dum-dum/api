package com.example.apiassignment.di

import android.content.Context
import com.example.apiassignment.ApiAssignment
import dagger.Module
import dagger.Provides
import javax.inject.Qualifier

@Module
class ApplicationModule {
    @ApplicationContext
    @Provides
    fun provideApplicationContext(apiAssignment: ApiAssignment): Context = apiAssignment.applicationContext
}

@Qualifier
annotation class ApplicationContext