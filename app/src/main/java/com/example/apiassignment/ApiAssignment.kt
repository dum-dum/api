package com.example.apiassignment

import com.example.apiassignment.di.DaggerApplicationComponents
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class ApiAssignment : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerApplicationComponents.builder().apiAssignment(this).build()
    }
}